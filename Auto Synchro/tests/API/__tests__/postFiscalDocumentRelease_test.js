const supertest = require('supertest');
const urls = require('../../config/base_urls_fiscalDocumentsRelease.json');
const gql = require('../../factory/postFiscalDocumentRelease_dto.js');
const tokens = require('../../config/token.json');
const order = require('../../functions/functions.js');
const request = supertest(urls.dit2);

const urls2 = require('../../config/base_urls_fiscalDocuments.json');
const gql2 = require('../../factory/postFiscalDocument_dto.js');
const request2 = supertest(urls2.dit2);

var orderType_DSA = "20";
var iaOrderGlobal = order.createOrderNumber();
var scac = "DELL"

describe('\n\nService (postFiscalDocumentRelease) validation', () => {

     test(`ID01 - Generate a order DSA Type`, async () => {
          process.env["NODE_TLS_REJECT_UNAUTHORIZED"] = 0;
              let res = await request2.post('/')
                   .set('Authorization', `Bearer ${tokens.personAPI}`)
                   .query({GenerateFinishGoodId: false, SendToFDLQueue: false, IsDomsOrder: false})
                   .send(gql2.allInputFields(orderType_DSA, iaOrderGlobal));
                   
                   expect(res.body.data).toBeUndefined();
                   expect(res.text).toContain(`${orderType_DSA}-${iaOrderGlobal}`);
                   console.log(res.text);
         },15000);
         
     test(`ID02 - Generate a product note requisition`, async () => {
          process.env["NODE_TLS_REJECT_UNAUTHORIZED"] = 0;
               let res = await request.post('/')
                    .set('Authorization', `Bearer ${tokens.personAPI}`)
                    .send(gql.allInputFields(iaOrderGlobal, scac));
     
                    /*expect(res.body.data).toBeFalsy();*/
                    console.log(res.body);
    });
});