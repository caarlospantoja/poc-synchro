const supertest = require('supertest');
const urls = require('../../config/base_urls_fiscalDocuments.json');
const gql = require('../../factory/postFiscalDocument_dto.js');
const tokens = require('../../config/token.json');
const order = require('../../functions/functions.js');
const request = supertest(urls.dit2);

var orderType_synchro = "02";
var orderType_DSA = "20";
var iaOrderGlobal = order.createOrderNumber();

describe('\n\nService (postFiscalDocument) validation', () => {

    test(`100221 - Generate a order SYNCHRO Type`, async () => {
      var iaOrderN = order.createOrderNumber();
      process.env["NODE_TLS_REJECT_UNAUTHORIZED"] = 0;
          let res = await request.post('/')
               .set('Authorization', `Bearer ${tokens.personAPI}`)
               .query({GenerateFinishGoodId: false, SendToFDLQueue: false, IsDomsOrder: false})
               .send(gql.allInputFields(orderType_synchro, iaOrderN));
               
               expect(res.body.data).toBeUndefined();
               expect(res.text).toContain(`${orderType_synchro}-${iaOrderN}`);
               console.log(res.text);
     });

     test(`100221 - Generate a order DSA Type`, async () => {
      process.env["NODE_TLS_REJECT_UNAUTHORIZED"] = 0;
          let res = await request.post('/')
               .set('Authorization', `Bearer ${tokens.personAPI}`)
               .query({GenerateFinishGoodId: false, SendToFDLQueue: false, IsDomsOrder: false})
               .send(gql.allInputFields(orderType_DSA, iaOrderGlobal));
               
               expect(res.body.data).toBeUndefined();
               expect(res.text).toContain(`${orderType_DSA}-${iaOrderGlobal}`);
               console.log(res.text);
     });
/*
    it("Generate a product note requisition", async () => {
     process.env["NODE_TLS_REJECT_UNAUTHORIZED"] = 0;
     return request(apiURL)
       .post("/")
       .set('Authorization', `Bearer ${TOKEN}`)
       .send({
          query: gql.allInputFields()
     })
          .expect(200)
 
 
       .then(response => {
         console.log(response.body);
       });
   },7000);*/
});