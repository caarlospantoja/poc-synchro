module.exports.allInputFields = function (laOrderNumber, scac) {
    return {
        "laOrderNumber": `${laOrderNumber}`,
        "scac": `${scac}`,
        "weight": 10.64,
        "volume": 2,
        "serviceTags": "6721X53",
        "sourceReferenceId": "BRH055465364",
        "buid": 11 
    };
};