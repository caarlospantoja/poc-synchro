module.exports.allInputFields = function (orderType, orderId) {
    return {
        "systemCode": `${orderType}`,
        "orderId": `${orderId}`,
        "series": "1",
        "shipping": 1,
        "duties": 638.87,
        "operationNature": "S02.50",
        "documentType": "NFE",
        "recipient": {
            "pfjCode": "100073446",
            "locationCode": "00000"
        },
        "issuer": {
            "pfjCode": "BFC1",
            "locationCode": "B0403"
        },
        "issuanceStatus": "C",
        "invoiceOperationType": "S",
        "taxBookInformantPfjCode": "BFC1",
        "freightDetails": {
            "freightAmount": 0,
            "freightMode": "1"
        },
        "otherExpensesAmount": 0,
        "payments": {
            "purchaseNumber": "PG VIA CITI",
            "paymentMethods": {
                "paymentMethod1": "P"
            }
        },
        "saler": {
            "salerName": "MASTER",
            "salerNumber": "09999"
        },
        "totalAmountLC": 327.00,
        "totalAmountUSD": 327.00,
        "systemFlag": "Y",
        "orderType": "5",
        "goodsAdjustmentAmount": 0,
        "additionalInformation": {
            "customerClassCode": "IE",
            "paymentInformation": {
                "amount": {
                    "paymentAmount1": 327.00
                }
            },
            "shippingInformation": 412.66,
            "dutyInformation": 638.87,
            "exchangeRate": 3.1501,
            "sourceTypeCode": "M"
        },
        "preFiscalDocumetItems": [
            {
                "sequentialNumber": 1,
                "goodsCode": "210-ADOK",
                "itemType": "M",
                "natureOfOperation": "S02.50",
                "baseFlag": "Y",
                "unitPrice": 1.55,
                "rawUnitPrice": 0,
                "quantity": 1,
                "dellTieNum": "001",
                "pricesIncludeIcms": "N",
                "orderItemAdditionalInformation": {
                    "itemLob": "60",
                    "itemUnitAsSoldPrice": 0,
                    "itemUnitListPrice": 0,
                    "itemUnitCostAmount": 0,
                    "tieNumber": "001",
                    "itemUnitCostAmountUsd": 0
                }
            }
        ]
    };
}